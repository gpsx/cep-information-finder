package gps.x.retrofit.model

//@JsonIgnoreProperties("codibge", "codestado")
data class CEP (
    var cep : String,
    var codibge : Int,
    var codestado : Int,
    var logradouro : String,
    var complemento : String,
    var bairro : String,
    var cidade: String,
    var estado: String
)
