package gps.x.retrofit.repository

import gps.x.retrofit.model.CEP
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CEPService {
    @GET("cep/find/{cep}/json")
    fun searchCEP(@Path("cep") cep: String): Call<CEP>
}