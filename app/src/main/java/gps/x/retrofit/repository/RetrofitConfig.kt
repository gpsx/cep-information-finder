package gps.x.retrofit.repository

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig {
    lateinit var retrofit: Retrofit
    init {
        retrofitConfig()
    }
    fun retrofitConfig(){
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        retrofit = Retrofit.Builder()
            .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
            .baseUrl("http://ws.matheuscastiglioni.com.br/ws/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getCEPService(): CEPService {
        return this.retrofit.create(CEPService::class.java)
    }

}