package gps.x.retrofit.repository

import gps.x.retrofit.controller.CEPController
import gps.x.retrofit.model.CEP
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object CEPRepository{
    fun getCEPDetails(searchCep : String, controller: CEPController) {
        val call : Call<CEP> = RetrofitConfig().getCEPService().searchCEP(searchCep)

        call.enqueue(object : Callback<CEP> {
            override fun onResponse(call: Call<CEP>, response: Response<CEP>) {

                if (response.isSuccessful)
                    controller.updateCEP(response.body()!!)
                else
                    controller.showErr("N erros podem acontecer")
            }

            override fun onFailure(call: Call<CEP>, t: Throwable) {
                controller.showErr("Problemas com a conexão ;_;")
            }
        })

    }
}