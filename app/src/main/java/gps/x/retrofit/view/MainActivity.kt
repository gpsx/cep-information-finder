package gps.x.retrofit.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.UiThread
import gps.x.retrofit.R
import gps.x.retrofit.controller.CEPController
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import gps.x.retrofit.model.CEP
import gps.x.retrofit.repository.RetrofitConfig


class MainActivity : AppCompatActivity() {

    private val cepController by lazy { CEPController(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            cepController.cepSearch(editText.text.toString())
        }
    }

    @UiThread
    fun updateScreen(cep: CEP){
        text_response.text = getString(
            R.string.cep_search_placeholder, cep.cep, cep.logradouro, cep.complemento, cep.bairro,
            cep.cidade,cep.estado)
    }

    @UiThread
    fun showError(text : String){
        text_response.text = getString(R.string.err, text)
    }
}
