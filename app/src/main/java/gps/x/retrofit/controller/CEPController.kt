package gps.x.retrofit.controller

import gps.x.retrofit.model.CEP
import gps.x.retrofit.repository.CEPRepository
import gps.x.retrofit.view.MainActivity


class CEPController(private val main : MainActivity) {
    private val cepRepository = CEPRepository

    fun cepSearch(cep : String) {
        if(cep.isEmpty()){
            showErr("Tá vazio irmão")
        }else{
            cepRepository.getCEPDetails(cep, this)
        }
    }

    fun updateCEP(cep : CEP){
        main.updateScreen(cep)
    }

    fun showErr(err : String){
        main.showError(err)
    }
}